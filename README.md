# Item Delete Check
![](https://img.shields.io/badge/Foundry-v10-informational)
![](https://img.shields.io/badge/Foundry-v11-informational)
![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fitem-delete-check&colorB=4aa94a)

Discord <a href="https://discord.gg/Xd4NEvw5d7"><img src="https://img.shields.io/discord/915186263609454632?logo=discord" alt="chat on Discord"></a>

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/tposney)

### A little module that does just ~~one~~ two things...
* When you click delete item from the actor inventory a confirmation dialog will appear before deleting the item. Can be enabled/disabled from settings.
* If notify GM setting (default off, changing requires reload) is enabled changes to inventory totals for all items (except feats/spells) will be whispered to the GM(s). The remaining numbers do not add across multiple copies of the same item.
* If notify GM Currencies is set (default off, changing requires reload) changes to player currencies will be whispered to the GM.  Currently only supports dnd5e. If there is a need for other systems add an issue to the gitlab or ping me in discord and I can add them.
* Turns out the messages can get quite annoying so there is a setting to only show them to the GM. 
* Changes can be logged to a journal as well as/instead of a chat message.
* The module is marked as not being system dependent, but it is only certain to work with a few systems, dnd5e, ose, Call of Cthulu (unofficial). Provided your character sheet is similar to the default dnd5e one, it may work for you.

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip]("https://gitlab.com/tposney/item-delete-check/raw/master/item-delete-check.zip) file included in the module directory. 
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  

Or (preferred)
Paste https://gitlab.com/tposney/item-delete-check/raw/master/module.json into the install module prompt inside Foundry.

