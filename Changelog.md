### 10.0.19
* fix for 0 quantity change messages
* Added pl.json

### 10.0.18
*  version bump for wrong zip file

### 10.0.17
* Fix for empty chat message in v12/dnd5e v4 (and possibly earlier)

### 10.0.16
* Marked as verified against 12.325
* Remove 0 changed reports for hp.
* Removed 0 change reports for currency

### 10.0.14
* Fix for error being thrown if simple calendar not installed.
* Added the ability to track hit point changes as well.

### 10.0.13
* Fix for OSE updates having the wrong sign.

### 10.0.12
* Fix for always reporting changes as the selected token.

### 10.0.11.1
* Fix typo in "Chat message for item changes".

### 10.0.11
* Setting to display a chat message and to update a journal are now independent. You can set either and they will work.
* Fix so that compatibility with DCC & OSE restored (removed some dnd5e dependencies).
* Journal entry pages are now GM private. You can grant players observer permission to individual pages if you want them to be able to see their page.
* If using item containers (the module) items added/deleted from a container owned by an actor will be logged to the chat/journal for that actor. 
  - Does not work for currency/changed quantities yet.

### 10.0.10
* **BREAKING Quite a few changes - you will have to respecify your settings.**
* Add the ability to report spell slot changes.
* Add the ability to blacklist items from being reported.
* Allow currency reporting to be enabled independently of other settings.
* Allow GM to specify a journal entry which will be created with one page per actor recording the same changes as sent to the chat log.
  - Changes logged to the journal will be timestamped with the simple calendar date/time (if installed) or a real world timestamp.
  - For linked tokens there is a page per actor.
  - For unlinked tokens all updates go into the "npc" page. Entries for unlinked actors have a link to the token in the journal page.
* Chatmessages/Journal entries have a link to the item that was updated/added.
  - if the item is subsequently deleted the links will display as invalid links.

### 10.0.9
- Fix for choosing actor only updates not displaying correctly.

### 10.0.7
* Don't update the inventory display when deleting an item - leave it to the character sheet to respond to the item deletion

### 10.0.6
* Fix for inventory changes by players not generating messages

### 10.0.5
* Added tracking of charges on items as well as item count.
* Enabled tracking changes for player characters only (actor type "character");
* Fix for hide messages from player setting not working (messages were always whispered);

### 10.0.4
* Removed system dependency from manifest

