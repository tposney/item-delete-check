let knownSheets = {
  BetterNPCActor5eSheet: ".item .rollable",
  ActorSheet5eCharacter: ".item .item-image",
  DynamicActorSheet5e: ".item .item-image",
  ActorSheet5eNPC: ".item .item-image",
  Sky5eSheet: ".item .item-image",
};
// ActorSheetPF2eCharacter: "",

let enableSheetQOL = (app, html, data) => {
  //Add a check for item deletion
  $(html).find(".item-delete").off("click");
  $(html).find(".item-delete").click({ app: app, data: data }, itemDeleteHandler);
};

let itemDeleteHandler = (ev) => {
  let actor = game.actors.get(ev.data.data.actor?._id ?? ev.data.data._id);
  let d = new Dialog({
    // localize this text
    title: game.i18n.localize("item-delete-check.reallyDelete"),
    content: `<p>${game.i18n.localize("item-delete-check.sure")}</p>`,
    buttons: {
      one: {
        icon: '<i class="fas fa-check"></i>',
        label: "Delete",
        callback: () => {
          let li = $(ev.currentTarget).parents(".item"),
            itemId = li.attr("data-item-id");
          ev.data.app.object.deleteEmbeddedDocuments("Item", [itemId]);
        },
      },
      two: {
        icon: '<i class="fas fa-times"></i>',
        label: "Cancel",
        callback: () => { },
      },
    },
    default: "two",
  });
  d.render(true);
};

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once("ready", function () {
  const currencySystems = ["dnd5e"];

  const templatePaths = [
    "modules/item-delete-check/templates/changes.html",
    "modules/item-delete-check/templates/journalChanges.html"
  ];

  loadTemplates(templatePaths);

  game.settings.register("item-delete-check", "CheckDelete", {
    name: "item-delete-check.CheckDelete.Name",
    hint: "item-delete-check.CheckDelete.Hint",
    scope: "world",
    default: true,
    type: Boolean,
    config: true,
    requiresReload: true,
  });

  game.settings.register("item-delete-check", "Notify", {
    name: "item-delete-check.Notify.Name",
    hint: "item-delete-check.Notify.Hint",
    scope: "world",
    default: false,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "JournalEntryName", {
    name: "item-delete-check.JournalEntryName.Name",
    hint: "item-delete-check.JournalEntryName.Hint",
    scope: "world",
    default: "",
    type: String,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "NotifyLinkedOnly", {
    name: "item-delete-check.NotifyLinkedOnly.Name",
    hint: "item-delete-check.NotifyLinkedOnly.Hint",
    scope: "world",
    default: true,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "HideMessage", {
    name: "item-delete-check.HideMessage.Name",
    hint: "item-delete-check.HideMessage.Hint",
    scope: "world",
    default: true,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "NotifyBlackList", {
    name: "item-delete-check.NotifyBlackList.Name",
    hint: "item-delete-check.NotifyBlackList.Hint",
    scope: "world",
    default: "",
    type: String,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "NotifyPrepared", {
    name: "item-delete-check.NotifyPrepared.Name",
    hint: "item-delete-check.NotifyPrepared.Hint",
    scope: "world",
    default: false,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  if (currencySystems.includes(game.system.id)) {
    game.settings.register("item-delete-check", "NotifyCurrency", {
      name: "item-delete-check.NotifyCurrency.Name",
      hint: "item-delete-check.NotifyCurrency.Hint",
      scope: "world",
      default: false,
      type: Boolean,
      config: true,
      requiresReload: false,
    });
  }

  game.settings.register("item-delete-check", "NotifySpellSlots", {
    name: "item-delete-check.NotifySpellSlots.Name",
    hint: "item-delete-check.NotifySpellSlots.Hint",
    scope: "world",
    default: false,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  game.settings.register("item-delete-check", "NotifyHitPoints", {
    name: "item-delete-check.NotifyHitPoints.Name",
    hint: "item-delete-check.NotifyHitPoints.Hint",
    scope: "world",
    default: false,
    type: Boolean,
    config: true,
    requiresReload: false,
  });

  console.log("item-delete-check | Initializing item-delete-check");
  if (game.settings.get("item-delete-check", "CheckDelete")) {
    for (let sheetName of Object.keys(knownSheets)) {
      Hooks.on("render" + sheetName, enableSheetQOL);
    }
    Object.keys(CONFIG.Actor.sheetClasses.character).forEach((name) => {
      let sheetName = name.split(".")[1];
      Hooks.on("render" + sheetName, enableSheetQOL);
    });
  }
  switch (game.system.id) {
    case "dnd5e":
      var currencyTypes = CONFIG.DND5E.currencies;
      break;
    default:
      var currencyTypes = {};
  }
  Hooks.on("preUpdateActor", (actor, update, ...args) => {
    if (game.settings.get("item-delete-check", "NotifyLinkedOnly") && actor.isToken) return;
    if (!game.settings.get("item-delete-check", "Notify") && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    if (currencySystems.includes(game.system.id) && game.settings.get("item-delete-check", "NotifyCurrency")) {
      if (update.system?.currency) {
        let originalCurrency = foundry.utils.duplicate(actor.system.currency);
        let currencyChanges = update.system.currency;
        Hooks.once("updateActor", () => {
          let content = [];
          let deltaCurrency = {};
          Object.keys(currencyChanges).forEach((cid) => {
            deltaCurrency[cid] = currencyChanges[cid] - originalCurrency[cid];
            if (deltaCurrency[cid] === 0) return;
            const deltaString = deltaCurrency[cid] >= 0 ? `+${deltaCurrency[cid]}` : `${deltaCurrency[cid]}`;
            let currencyType = currencyTypes[cid] ?? cid;
            if (typeof currencyType !== "string") currencyType = currencyType.label;
            content.push({ delta: deltaCurrency[cid], deltaString, final: currencyChanges[cid], item: currencyType });
            // `<p>${gainLoss}${Math.abs(deltaCurrency[cid])} ${currencyType} -> ${currenchChanges[cid]}</p>`);
          });
          if (foundry.utils.isEmpty(deltaCurrency)) return true;
          if (content.length === 0) return true;
          const speaker = ChatMessage.getSpeaker({ actor });
          const chatData = { speaker };
          if (game.settings.get("item-delete-check", "HideMessage")) {
            chatData.whisper = ChatMessage.getWhisperRecipients("GM");
            chatData.blind = true;
          }
          logChange(chatData, { parent: actor, source: "pre update actor 1" }, content);
          return true;
        });
      }
    }
    if (update.system?.spells && game.settings.get("item-delete-check", "NotifySpellSlots")) {
      let content = [];
      if (update.system.spells.pact !== undefined) {
        let originalSpells = actor.system.spells.pact?.value || 0;
        let newSpells = update.system.spells.pact.value;
        let consumed = originalSpells - newSpells;
        const deltaString = consumed >= 0 ? `+${consumed}` : `${consumed}`;
        if (consumed !== 0) content.push({ delta: consumed, deltaString, final: newSpells, item: "Pact Magic" });
        // if (consumed !== 0) content += `<p>${consumed > 0 ? "+" : "-"}${Math.abs(consumed)} pact magic spells -> ${newSpells}</p>`;
      }
      const levelName = {
        0: "zeroth",
        1: "first",
        2: "second",
        3: "third",
        4: "fourth",
        5: "fifth",
        6: "sixth",
        7: "seventh",
        8: "eighth",
        9: "ninth"
      }
      for (let level = 0; level < 10; level++) {
        if (update.system.spells[`spell${level}`]?.value !== undefined) {
          let originalSpells = actor.system.spells[`spell${level}`]?.value || 0;
          let newSpells = update.system.spells[`spell${level}`].value;
          let consumed = newSpells - originalSpells;
          if (consumed === 0) continue;
          const deltaString = consumed >= 0 ? `+${consumed}` : `${consumed}`;
          content.push({ delta: consumed, deltaString, final: newSpells, item: `${levelName[level]} level spells` });
          // content += `<p>${consumed > 0 ? "+" : "-"}${Math.abs(consumed)} ${levelName[level]} level spells -> ${newSpells}</p>`;
        }
      }
      if (content.length > 0) {
        const chatData = {
          speaker: ChatMessage.getSpeaker({ actor }),
          "flags": { "item-delete-check": { actorUuid: actor.uuid, originalSpells: actor.system.spells } },
        };
        if (game.settings.get("item-delete-check", "HideMessage")) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM");
          chatData.blind = true;
        }
        logChange(chatData, { parent: actor, source: " pre update actor 2" }, content);
      }
    }
    if (update.system?.attributes?.hp?.value !== undefined && game.settings.get("item-delete-check", "NotifyHitPoints")) {
      let originalHP = actor.system.attributes.hp.value;
      let newHP = update.system.attributes.hp.value;
      let consumed = newHP - originalHP;
      if (consumed !== 0) {
        const deltaString = consumed >= 0 ? `+${consumed}` : `${consumed}`;
        const content = [{ delta: consumed, deltaString, final: newHP, item: "Hit Points" }];
        const chatData = { speaker: ChatMessage.getSpeaker({ actor }) };
        if (game.settings.get("item-delete-check", "HideMessage")) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM");
          chatData.blind = true;
        }
        logChange(chatData, { parent: actor }, content);
      }
    }
    return true;
  });

  Hooks.on("deleteItem", (item, options, userId) => {
    if (!game.settings.get("item-delete-check", "Notify") && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    let parent = item.parent;
    while (!(parent instanceof Actor || !parent instanceof Token) && parent?.parent)
      parent = parent.parent;
    if (parent instanceof Token) parent = parent.actor;
    if (!(parent instanceof Actor)) return;
    if (game.settings.get("item-delete-check", "NotifyLinkedOnly") && item.parent.isToken)
      return;
    const user = game.users.get(userId);
    if (user.id !== game.user.id) return;
    if (!parent || options.temporary) return true;
    if (game.settings.get("item-delete-check", "NotifyBlackList") !== "") {
      const blackList = game.settings.get("item-delete-check", "NotifyBlackList").split(",").map(s => s.trim())
      if (blackList.some(s => item.name.toLowerCase().includes(s.toLowerCase()))) return;
    }
    // if (["spell", "feat"].includes(item.type)) return true;
    const speaker = ChatMessage.getSpeaker({ actor: parent });
    let originalQuantity = item.system.quantity || 0;
    if (typeof item.system.quantity === "object") {
      originalQuantity = item.system.quantity.value || 0;
    }
    let changes = [{ delta: originalQuantity, deltaString: `-${originalQuantity}`, final: 0, item: `${item.name}` }];
    if (!(item.parent instanceof Actor))
      changes = [{ delta: originalQuantity, deltaString: `-${originalQuantity}`, final: 0, item: `@UUID[${item.parent?.uuid}]{${item.parent?.name}}.${item.name}` }];
    const chatData = { speaker };
    if (game.settings.get("item-delete-check", "HideMessage")) {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM");
      chatData.blind = true;
    }
    logChange(chatData, { parent, source: "delete item" }, changes);
    return true;
  });

  Hooks.on("createItem", (item, options, userId) => {
    if (!game.settings.get("item-delete-check", "Notify") && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    const user = game.users.get(userId);
    if (user.id !== game.user.id) return;
    if (!item.parent || !(item.parent instanceof Actor) || options.temporary) return;
    const blackList = game.settings.get("item-delete-check", "NotifyBlackList").split(",").map(s => s.trim())
    if (game.settings.get("item-delete-check", "NotifyBlackList") !== "") {
      const blackList = game.settings.get("item-delete-check", "NotifyBlackList").split(",").map(s => s.trim())
      if (blackList.some(s => item.name.toLowerCase().includes(s.toLowerCase()))) return;
    }
    if (game.settings.get("item-delete-check", "NotifyLinkedOnly") && item.parent?.isToken)
      return;
    // if (["spell", "feat"].includes(item.type)) return;
    let originalQuantity = item.system.quantity || 0;
    if (typeof item.system.quantity === "object") {
      originalQuantity = item.system.quantity.value || 0;
    }
    const speaker = ChatMessage.getSpeaker({ actor: item.parent });
    const changes = [{ delta: originalQuantity, deltaString: `+${originalQuantity}`, final: originalQuantity, item: `@UUID[${item.uuid}]{${item.name}}` }];
    const chatData = { speaker };
    if (game.settings.get("item-delete-check", "HideMessage")) {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM");
      chatData.blind = true;
    }
    logChange(chatData, { parent: item.parent, source: "create item" }, changes);
    return true;
  });

  Hooks.on("createItemData", (item, itemData, options, userId) => {
    let parent = item.parent;
    while (!(parent instanceof Actor || !parent instanceof Token) && parent?.parent)
      parent = parent.parent;
    if (parent instanceof Token) parent = parent.actor;
    if (!(parent instanceof Actor)) return;
    // This is called by item containers.
    if (!game.settings.get("item-delete-check", "Notify") && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    const user = game.users.get(userId);
    if (user.id !== game.user.id) return;
    if (
      game.settings.get("item-delete-check", "NotifyLinkedOnly") &&
      !["character"].includes(parent.type)
    )
      return;
    if (["spell", "feat"].includes(itemData.type)) return;
    let originalQuantity = itemData.system.quantity || 0;
    if (typeof itemData.system.quantity === "object") {
      originalQuantity = itemData.system.quantity.value || 0;
    }

    const speaker = ChatMessage.getSpeaker({ actor: parent });
    const changes = [{ delta: originalQuantity, deltaString: `+${originalQuantity}`, final: originalQuantity, item: `@UUID[${item.uuid}]{${item.name}}.${itemData.name}` }];
    // const changes = `<p>@UUID[${item.uuid}]{${item.name}} New ${originalQuantity} ${itemData.name}</p>`;
    const chatData = { speaker };
    if (game.settings.get("item-delete-check", "HideMessage")) {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM");
      chatData.blind = true;
    }
    logChange(chatData, { parent: parent ?? item, source: "create item data" }, changes);
    return true;
  });

  Hooks.on("preUpdateItem", (item, update, options, userId) => {
    if (!game.settings.get("item-delete-check", "Notify")
      && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    if (!item.parent) return true;
    const blackList = game.settings.get("item-delete-check", "NotifyBlackList").split(",").map(s => s.trim())
    if (game.settings.get("item-delete-check", "NotifyBlackList") !== "") {
      const blackList = game.settings.get("item-delete-check", "NotifyBlackList").split(",").map(s => s.trim())
      if (blackList.some(s => item.name.toLowerCase().includes(s.toLowerCase()))) return;
    }
    if (!item.parent || !(item.parent instanceof Actor)) return;
    if (game.settings.get("item-delete-check", "NotifyLinkedOnly") && item.parent.isToken) return;
    let changes = [];
    if ((update.system?.quantity !== undefined && item.system.quantity !== update.system.quantity)
      || (update.system?.uses?.value !== undefined && item.system.uses.value !== update.system.uses.value)) {
      let quantityConsumed;
      let chargesConsumed;
      if (update.system?.quantity !== undefined) {
        let originalQuantity = item.system.quantity || 0;
        let newQuantity = update.system.quantity;
        quantityConsumed = update.system.quantity - originalQuantity;
        if (typeof item.system.quantity === "object") {
          originalQuantity = item.system.quantity.value || 0;
          newQuantity = update.system.quantity.value;
          quantityConsumed = newQuantity - originalQuantity;
        }
        const deltaString = quantityConsumed >= 0 ? `+${quantityConsumed}` : `${quantityConsumed}`;
        changes.push({ delta: quantityConsumed, deltaString, final: newQuantity, item: `@UUID[${item.uuid}]{${item.name}}` });
        // changes += `<p>${quantityConsumed > 0 ? "-" : "+"}${Math.abs(quantityConsumed)} @UUID[${item.uuid}]{${item.name}} -> ${newQuantity}</p>`;
      }

      if (update.system?.uses?.value !== undefined) {
        let originalCharges = item.system.uses?.value || 0;
        let newCharges = update.system.uses.value;
        const chargesConsumed = newCharges - originalCharges;
        const deltaString = chargesConsumed >= 0 ? `+${chargesConsumed}` : `${chargesConsumed}`;
        changes.push({ delta: chargesConsumed, deltaString, final: newCharges, item: `@UUID[${item.uuid}]{${item.name}}` })
        // changes += `<p>${chargesConsumed > 0 ? "-" : "+"}${Math.abs(chargesConsumed)} charges - @UUID[${item.uuid}]{${item.name}} -> ${newCharges}</p>`;
      }
      Hooks.once("updateItem", () => {
        const speaker = ChatMessage.getSpeaker({ actor: item.parent });
        const chatData = { speaker };
        if (game.settings.get("item-delete-check", "HideMessage")) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM");
          chatData.blind = true;
        }
        foundry.utils.setProperty(chatData, "flags.item-delete-check", { itemUuid: item.uuid, actorUuid: item.parent.uuid, quantityConsumed, chargesConsumed })
        logChange(chatData, { parent: item.parent, source: " pre update item" }, changes);
      });
    }
    return true;
  });
  Hooks.on("preUpdateItem", (item, update, options, userId) => {
    if (!game.settings.get("item-delete-check", "Notify") && game.settings.get("item-delete-check", "JournalEntryName") === "") return;
    if (!game.settings.get("item-delete-check", "NotifyPrepared")) return;
    if (!item.parent) return true;
    if (game.settings.get("item-delete-check", "NotifyLinkedOnly") && item.parent.type !== "character")
      return;
    let content = "";
    if (update.system?.preparation?.prepared !== undefined && item.system.preparation?.prepared !== update.system.preparation.prepared) {
      content = `<p>${getDateTime(SVGComponentTransferFunctionElement)} ${update.system.preparation.prepared ? "Prepared" : "Unprepared"} @UUID[${item.uuid}]{${item.name}}`;

      const speaker = ChatMessage.getSpeaker({ actor: item.parent });
      const chatData = { speaker, content };
      if (game.settings.get("item-delete-check", "HideMessage")) {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM");
        chatData.blind = true;
      }
      logChange(chatData, { parent: item.parent, source: "pre update item" }, []);
    }
    return true;
  });
});

async function getJournal() {
  let journal = game.journal.getName(game.settings.get("item-delete-check", "JournalEntryName"));
  if (!journal) {
    await JournalEntry.create({
      name: game.settings.get("item-delete-check", "JournalEntryName"),
      content: "",
      folder: null,
      permission: { default: CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER },
      flags: {},
    });
    journal = game.journal.getName(game.settings.get("item-delete-check", "JournalEntryName"));
  }
  return journal;
}
let socketlibSocket;
async function logChange(chatData, options, itemsToRender) {
  let content = foundry.utils.duplicate(chatData.content ?? "");
  if (itemsToRender?.length > 0) {
    const sheet = "modules/item-delete-check/templates/changes.html";
    const html = await renderTemplate(sheet, { changes: itemsToRender });
    chatData.content = content + html;
  }
  if (game.settings.get("item-delete-check", "Notify")) await ChatMessage.create(chatData);
  if (game.settings.get("item-delete-check", "JournalEntryName") !== "") {
    let journalContent = "";
    const pageToUse = options.parent?.isToken ? "npc" : chatData.speaker.alias;
    let name = "";
    let uuidLink = "";
    if (pageToUse === "npc") {
      name = `@UUID[${options.parent.uuid}]{${options.parent.name}} `;
    } else uuidLink = `<p>@UUID[${options.parent?.uuid}]{${options.parent?.name}}</p>`

    if (itemsToRender?.length > 0) {
      const journalSheet = "modules/item-delete-check/templates/journalChanges.html";
      journalContent = await renderTemplate(journalSheet, { name, dateTime: getDateTime(true), changes: itemsToRender });
    }
    await socketlibSocket.executeAsGM("logToJournal", { pageToUse, content, journalContent, options, uuidLink });
  }
}

Hooks.once("socketlib.ready", () => {
  socketlibSocket = globalThis.socketlib.registerModule("item-delete-check");
  socketlibSocket.register("logToJournal", logToJournal);
});

async function logToJournal(data) {
  const journalEntry = await getJournal();

  let page = journalEntry.pages.getName(data.pageToUse);
  if (!page) {
    return CONFIG.JournalEntryPage.documentClass.create({
      name: data.pageToUse,
      text: { content: data.uuidLink + data.content + data.journalContent },
      ownership: { default: CONST.DOCUMENT_OWNERSHIP_LEVELS.NONE },
      flags: {},
    }, { parent: journalEntry });
  } else {
    return page.update({ text: { content: page.text.content + data.content + data.journalContent } });
  }
}
function getDateTime(useSC = true) {
  if (globalThis.SimpleCalendar?.api) {
    const scDateTime = SimpleCalendar.api.formatTimestamp(SimpleCalendar.api.timestamp());
    return `${scDateTime.date} ${scDateTime.time}`;
  } else return new Date().toLocaleString();
}
